package com.mendez.b142.s02.s02app.services;

import com.mendez.b142.s02.s02app.models.User;

import java.util.Optional;

public interface UserService {
    void createUser(User newUser);
    void updateUser(Long id, User updatedUser);
    void deleteUser(Long id);
    Iterable<User> getUsers();
    Optional<User> findByUsername(String username);
}